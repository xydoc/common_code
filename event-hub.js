import Vue from 'vue'
// import router from './router'

const $eventHub = Vue.prototype.$eventHub || new Vue()
Vue.prototype.$eventHub = $eventHub

$eventHub.$on('switchTab', function (msg) {
  console.log(msg)
})

$eventHub.$emit('switchTab', 'hi')
